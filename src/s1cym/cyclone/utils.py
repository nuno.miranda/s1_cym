from shapely.geometry import LineString, MultiLineString, Point, MultiPoint, Polygon

from pyproj import Geod

def move_point(point:Point ,heading:float, velocity:float, duration:float ):
    '''
        Starting from an initial position defined a shapely Point, this function finds a
        final point terminating a route defined by a heading, a speed and a duration

        Parameters
        ----------
        Point: degrees
            coordinate of the initial point in lon, lat over the WGS84 ellipsoid 
        float: Eastward heading in degrees
            0º degrees corresponds to East direction
            90º south
            180º  west
            270º  north
        foat: velociy in m/s
        float: duration in seconds

        Returns:
        --------
        Point : final point geographic coordinates
        

    
    '''
    geod = Geod(ellps='WGS84')  # Ellipsoid model to use for geographic calculations

    start_lon, start_lat = point.x, point.y
    
    # Compute the initial azimuth between starting point and eastward direction
    azimuth, back_azimuth_degrees, distance_meters = geod.inv(start_lon, start_lat, start_lon+0.01, start_lat)
    azimuth = (azimuth + 360) % 360

    # Compute the final point coordinates after moving for the specified duration
    azimuth_full = azimuth + heading
    final_lon, final_lat, dum = geod.fwd(start_lon, start_lat, azimuth_full, velocity*duration)
    
    final_point = Point(final_lon, final_lat)
    return  final_point


def heading( point0,  point1):
    """
    Compute the Eastward heading
    """
    geod = Geod(ellps='WGS84')  # Ellipsoid model to use for geographic calculations
    start_lon, start_lat = point0.x, point0.y
    end_lon, end_lat = point1.x, point1.y
    
    # Compute the distance using the haversine formula
    hv = geod.inv(start_lon, start_lat, end_lon, end_lat)
    head = hv[0]
    distance = hv[2]
    return  head, distance

def pass_generator(swath:LineString, velocity=7000, duration=30):

    pt0 = Point(swath.coords[0])
    pt1 = Point(swath.coords[-1])
    hd, dd = heading(pt0, pt1)
    print (swath,  hd, dd)

    pt0u = move_point(pt0, hd+90-270, velocity,duration)
    pt1u = move_point(pt1, hd+90-270, velocity,duration)
    pt0d = move_point(pt0, hd-90-270, velocity,duration)
    pt1d = move_point(pt1, hd-90-270, velocity,duration)

    p = Polygon( [pt0,  pt0u, pt1u, pt1, pt1d,pt0d, pt0 ] )
    return p