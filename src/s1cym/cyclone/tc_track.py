from typing import Optional, List, Union
from urllib.parse import urlparse, urlunparse
from pathlib import Path
from datetime import date, timedelta, datetime
from  dateutil.parser import parse
import itertools
import fsspec
import numpy as np
import xarray as xr
import eccodes as ec
import re

from .logger._logger import get_logger

logger = get_logger(__name__)


SAFFIR_MS_CAT = np.array([18, 33, 43, 50, 59, 71, 1000])
"""Saffir-Simpson Hurricane Categories in m/s"""

SAFFIR_SIM_CAT = [34, 64, 83, 96, 113, 137, 1000]
"""Saffir-Simpson Hurricane Wind Scale in kn based on NOAA"""

CAT_NAMES = {
    -1: 'Tropical Depression',
    0: 'Tropical Storm',
    1: 'Hurricane Cat. 1',
    2: 'Hurricane Cat. 2',
    3: 'Hurricane Cat. 3',
    4: 'Hurricane Cat. 4',
    5: 'Hurricane Cat. 5',
}
MISSING_DOUBLE = ec.CODES_MISSING_DOUBLE
MISSING_LONG = ec.CODES_MISSING_LONG
DEF_ENV_PRESSURE = 1010

MODEL_TIMES = ['000000','060000', '120000', '180000']



ECMWF_URL_BASE = 'https://diss.ecmwf.int/ecpds/home/wmo/'
ECMWF_USER ='wmo'
ECMWF_PASSWORD = 'essential',

    
class TCTracks(list):
    """Contains tropical cyclone tracks.

    Attributes
    ----------
    data : list(xarray.Dataset)
        List of tropical cyclone tracks. Each track contains following attributes:
            - time (coords)
            - lat (coords)
            - lon (coords)
            - time_step (in hours)
            - radius_max_wind (in nautical miles)
            - radius_oci (in nautical miles)
            - max_sustained_wind (in knots)
            - central_pressure (in hPa/mbar)
            - environmental_pressure (in hPa/mbar)
            - basin (for each track position)
            - max_sustained_wind_unit (attrs)
            - central_pressure_unit (attrs)
            - name (attrs)
            - sid (attrs)
            - orig_event_flag (attrs)
            - data_provider (attrs)
            - id_no (attrs)
            - category (attrs)
        Computed during processing:
            - on_land (bool for each track position)
            - dist_since_lf (in km)
    """
    


    def wmax_ensemble(self):
        tcmax = self.__getitem__(0)
        for track in self:
            tcmax = track if track.max_sustained_wind.max().values > tcmax.max_sustained_wind.max().values else tcmax
        return tcmax    
        
    @staticmethod
    def download_bufr( url_list:List, out_dir:Union[Path, str]='./'):

        out_dir = Path(out_dir)

        if not out_dir.exists():
            logger.info (f"Creating output directory : {out_dir}")
            out_dir.mkdir(parents=True, exist_ok=True)

        if not isinstance(url_list, list):
            url_list = [url_list]
        
        parsed_url = urlparse(url_list[0])
        
        fs = fsspec.filesystem(protocol=parsed_url.scheme )

        local_file_list=[]
        for f_ in url_list:
            f_local = out_dir / Path(f_).name

            with fs.open(f_) as inp:
                with open(f_local, 'wb') as out:
                    out.write( inp.read())
                    local_file_list.append(f_local)

        logger.info(f"Written {len(local_file_list)} file(s) on disk")
        return local_file_list


    @classmethod
    def from_ecmwf_bufr(cls, input_file, id_no=None):
        """ Read a single BUFR TC track file tailored to the ECMWF TC track
        predictions format.

        Parameters:
            file (str, filelike): Path object, string, or file-like object
            id_no (int): Numerical ID; optional. Else use date + random int.
        """
        # Open the bufr file
        #if isinstance(file, str) or isinstance(file, Path):
        #    # for the case that file is str, try open it
        #    file = open(file, 'rb')
        
        with fsspec.open(f"filecache::{input_file}",filecache={'cache_storage':'/tmp/files'}) as file:
            bufr = ec.codes_bufr_new_from_file(file)

            # we need to instruct ecCodes to expand all the descriptors
            # i.e. unpack the data values
            ec.codes_set(bufr, 'unpack', 1)

            # get the forecast time
            timestamp_origin = datetime(
                ec.codes_get(bufr, 'year'), ec.codes_get(bufr, 'month'),
                ec.codes_get(bufr, 'day'), ec.codes_get(bufr, 'hour'),
                ec.codes_get(bufr, 'minute'),
            )
            timestamp_origin = np.datetime64(timestamp_origin)

            # get storm identifier
            sid = ec.codes_get(bufr, 'stormIdentifier').strip()

            # number of timesteps (size of the forecast time + initial analysis timestep)
            try:
                n_timestep = ec.codes_get_size(bufr, 'timePeriod') + 1
            except ec.CodesInternalError:
                logger.warning("Track %s has no defined timePeriod. Track is discarded.", sid)
                return None

            # get number of ensemble members
            ens_no = ec.codes_get_array(bufr, "ensembleMemberNumber")
            n_ens = len(ens_no)
            logger.info(f"Number of ensemble number : {n_ens}")
            # See documentation for link to ensemble types
            # Sometimes only one value is given instead of an array and it needs to be reproduced across all tracks
            ens_type = ec.codes_get_array(bufr, 'ensembleForecastType')
            if len(ens_type) == 1:
                ens_type = np.repeat(ens_type, n_ens)

            # values at timestep 0 (perturbed from the analysis for each ensemble member)
            lat_init_temp = ec.codes_get_array(bufr, '#2#latitude')
            lon_init_temp = ec.codes_get_array(bufr, '#2#longitude')
            pre_init_temp = ec.codes_get_array(bufr, '#1#pressureReducedToMeanSeaLevel')
            wnd_init_temp = ec.codes_get_array(bufr, '#1#windSpeedAt10M')

            # check dimension of the variables, and replace missing value with NaN
            lat_init = cls._check_variable(lat_init_temp, n_ens, varname="Latitude at time 0")
            lon_init = cls._check_variable(lon_init_temp, n_ens, varname="Longitude at time 0")
            pre_init = cls._check_variable(pre_init_temp, n_ens, varname="Pressure at time 0")
            wnd_init = cls._check_variable(wnd_init_temp, n_ens, varname="Maximum 10m wind at time 0")

            # Create dictionaries of lists to store output for each variable.
            # Each dict entry is an ensemble member, and it contains a list of forecast values by timestep
            latitude = {ind_ens: np.array(lat_init[ind_ens]) for ind_ens in range(n_ens)}
            longitude = {ind_ens: np.array(lon_init[ind_ens]) for ind_ens in range(n_ens)}
            pressure = {ind_ens: np.array(pre_init[ind_ens]) for ind_ens in range(n_ens)}
            max_wind = {ind_ens: np.array(wnd_init[ind_ens]) for ind_ens in range(n_ens)}

            # getting the forecasted storms
            timesteps_int = [0 for x in range(n_timestep)]

            tracks = cls()

            for ind_timestep in range(1, n_timestep):
                rank1 = ind_timestep * 2 + 2  # rank for getting storm centre information
                rank3 = ind_timestep * 2 + 3  # rank for getting max wind information

                # Get timestep
                timestep = ec.codes_get_array(bufr, "#%d#timePeriod" % ind_timestep)
                timesteps_int[ind_timestep] = cls.get_value_from_bufr_array(timestep)

                # Location of the storm: first check significance value matches what we expect
                sig_values = ec.codes_get_array(bufr, "#%d#meteorologicalAttributeSignificance" % rank1)
                significance = cls.get_value_from_bufr_array(sig_values)

                # get lat, lon, and pressure of all ensemble members at ind_timestep
                if significance == 1:
                    lat_temp = ec.codes_get_array(bufr, "#%d#latitude" % rank1)
                    lon_temp = ec.codes_get_array(bufr, "#%d#longitude" % rank1)
                    pre_temp = ec.codes_get_array(bufr, "#%d#pressureReducedToMeanSeaLevel" % (ind_timestep + 1))
                else:
                    raise ValueError('unexpected meteorologicalAttributeSignificance=', significance)

                # Location of max wind: check significance value matches what we expect
                sig_values = ec.codes_get_array(bufr, "#%d#meteorologicalAttributeSignificance" % rank3)
                significanceWind = cls.get_value_from_bufr_array(sig_values)

                # max_wind of each ensemble members at ind_timestep
                if significanceWind == 3:
                    wnd_temp = ec.codes_get_array(bufr, "#%d#windSpeedAt10M" % (ind_timestep + 1))
                else:
                    raise ValueError('unexpected meteorologicalAttributeSignificance=', significance)

                # check dimension of the variables, and replace missing value with NaN
                lat = cls._check_variable(lat_temp, n_ens, varname="Latitude at time "+str(ind_timestep))
                lon = cls._check_variable(lon_temp, n_ens, varname="Longitude at time "+str(ind_timestep))
                pre = cls._check_variable(pre_temp, n_ens, varname="Pressure at time "+str(ind_timestep))
                wnd = cls._check_variable(wnd_temp, n_ens, varname="Maximum 10m wind at time "+str(ind_timestep))

                # appending values into dictionaries
                for ind_ens in range(n_ens):
                    latitude[ind_ens] = np.append(latitude[ind_ens], lat[ind_ens])
                    longitude[ind_ens] = np.append(longitude[ind_ens], lon[ind_ens])
                    pressure[ind_ens] = np.append(pressure[ind_ens], pre[ind_ens])
                    max_wind[ind_ens] = np.append(max_wind[ind_ens], wnd[ind_ens])

            # storing information into a dictionary
            msg = {
                # subset forecast data
                'latitude': latitude,
                'longitude': longitude,
                'wind_10m': max_wind,
                'pressure': pressure,
                'timestamp': timesteps_int,

                # subset metadata
                'wmo_longname': ec.codes_get(bufr, 'longStormName').strip(),
                'storm_id': sid,
                'ens_type': ens_type,
                'ens_number': ens_no,
            }

            if id_no is None:
                id_no = timestamp_origin.item().strftime('%Y%m%d%H') + \
                        str(np.random.randint(1e3, 1e4))

            orig_centre = ec.codes_get(bufr, 'centre')
            if orig_centre == 98:
                provider = 'ECMWF'
            else:
                provider = 'BUFR code ' + str(orig_centre)

            
            for i in range(n_ens):
                name = msg['wmo_longname']
                track = cls._subset_to_track(
                    msg, i, provider, timestamp_origin, name, id_no
                )
                logger.info(f"TC {track.name} from {track.run_datetime} : [{track.time[0].values}, {track.time[-1].values}]")
                if track is not None:
                    tracks.append(track)
                else:
                    logger.debug('Dropping empty track %s, subset %d', name, i)
        return tracks
            
    @staticmethod
    def get_tropical_storm_name(file_list,  pattern='[A-Z]{1}_.+_[C]_[A-Z]{4}_[0-9]{14}.+\.bin'):
        
        storm_name = []
        for file in file_list:
            file = Path(file).name
            match = re.search(pattern, file)

            if match:
                s = re.split('_|\.bin', file)
                storm_name.append(s[8])
        return storm_name
    
    @staticmethod
    def get_active_tropical_cyclones(days_back=0, model_times=None) ->dict:
        """
        CHeck if there are active cyclones
        Returns a dict being cyclone name : url
        Return
        ------
        dict: mapping name : url
        """
        urls = TCTracks.check_last_forecast(pattern='ECM[F].+tropical_cyclone',
                                            ref_date=date.today(),
                                            days_back=days_back,
                                            model_times=model_times)
        
        names = TCTracks.get_tropical_storm_name(urls)
        active = {}
        for name, url in zip(names, urls):
            if not name[0:2].isdigit():
                active[name] =url
        logger.info (f"Found active cyclones {active}")
        return active
    
    @staticmethod
    def get_all_tropical_cyclones(days_back=0, model_times=None) ->dict:
        """
        CHeck if there are  cyclones
        Returns a dict being cyclone name : url
        Return
        ------
        dict: mapping name : url
        """
        urls = TCTracks.check_last_forecast(pattern='ECM[F].+tropical_cyclone',
                                            ref_date=date.today(),
                                            days_back=days_back, 
                                            model_times=model_times)
        names = TCTracks.get_tropical_storm_name(urls)
        active = {}
        for name, url in zip(names, urls):
            active[name] = url
        logger.info (f"Found active cyclones {active}")
        return active

    @staticmethod
    def get_value_from_bufr_array(var):
        for v_i in var:
            if v_i != MISSING_LONG:
                return v_i
        raise ValueError("Array contained a single, missing value") if len(var) == 1 \
            else ValueError("Did not find a non-missing value in the array")

    @staticmethod
    def _check_variable(var, n_ens, varname=None):
        """Check the value and dimension of variable"""
        if len(var) == n_ens:
            var[var == MISSING_DOUBLE] = np.nan
            return var
        elif len(var) == 1 and var[0] == MISSING_DOUBLE:
            return np.repeat(np.nan, n_ens)
        elif len(var) == 1 and var[0] != MISSING_DOUBLE:
            logger.warning('%s: only 1 variable value for %d ensemble members, duplicating value to all members. '
                           'This is only acceptable for lat and lon data at time 0.', varname, n_ens)
            return np.repeat(var[0], n_ens)

        else:
            raise ValueError

    @staticmethod
    def _subset_to_track(msg, index, provider, timestamp_origin, name, id_no):
        """Subroutine to process one BUFR subset into one xr.Dataset"""
        lat = np.array(msg['latitude'][index], dtype='float')
        lon = np.array(msg['longitude'][index], dtype='float')
        wnd = np.array(msg['wind_10m'][index], dtype='float')
        pre = np.array(msg['pressure'][index], dtype='float')

        sid = msg['storm_id'].strip()

        timestep_int = np.array(msg['timestamp']).squeeze()
        timestamp = timestamp_origin + timestep_int.astype('timedelta64[h]')

        # 'ens_type' can take a number of values telling us the source of the track.
        # 0 means the deterministic analysis, which we want to flag.
        # See documentation for link to ensemble types.
        ens_bool = msg['ens_type'][index] != 0

        try:
            track = xr.Dataset(
                data_vars={
                    'max_sustained_wind': ('time', np.squeeze(wnd)),
                    'central_pressure': ('time', np.squeeze(pre)/100),
                    'ts_int': ('time', timestep_int),
                },
                coords={
                    'time': timestamp,
                    'lat': ('time', lat),
                    'lon': ('time', lon),
                },
                attrs={
                    'max_sustained_wind_unit': 'm/s',
                    'central_pressure_unit': 'mb',
                    'name': name,
                    'sid': sid,
                    'orig_event_flag': False,
                    'data_provider': provider,
                    'id_no': (int(id_no) + index / 100),
                    'ensemble_number': msg['ens_number'][index],
                    'is_ensemble': ens_bool,
                    'run_datetime': timestamp_origin,
                }
            )
        except ValueError as err:
            logger.warning(
                'Could not process track %s subset %d, error: %s',
                sid, index, err
                )
            return None

        track = track.dropna('time')

        if track.sizes['time'] == 0:
            return None

        # can only make latlon coords after dropna
        track = track.set_coords(['lat', 'lon'])
        track['time_step'] = track.ts_int - \
            track.ts_int.shift({'time': 1}, fill_value=0)

        track = track.drop_vars(['ts_int'])

        track['radius_max_wind'] = (('time'), np.full_like(
            track.time, np.nan, dtype=float)
        )
        track['environmental_pressure'] = (('time'), np.full_like(
            track.time, DEF_ENV_PRESSURE, dtype=float)
        )

        # according to specs always num-num-letter
        track['basin'] = ('time', np.full_like(track.time, sid[2], dtype='<U2'))

        if sid[2] == 'X':
            logger.info(
                'Undefined basin %s for track name %s ensemble no. %d',
                sid[2], track.attrs['name'], track.attrs['ensemble_number'])

        cat_name = CAT_NAMES[TCTracks.set_category(
            max_sus_wind=track.max_sustained_wind.values,
            wind_unit=track.max_sustained_wind_unit,
            saffir_scale=SAFFIR_MS_CAT
        )]
        track.attrs['category'] = cat_name
        return track


    
    @staticmethod
    def set_category(max_sus_wind, wind_unit='kn', saffir_scale=None):
        """Add storm category according to Saffir-Simpson hurricane scale.

        Parameters
        ----------
        max_sus_wind : np.array
            Maximum sustained wind speed records for a single track.
        wind_unit : str, optional
            Units of wind speed. Default: 'kn'.
        saffir_scale : list, optional
            Saffir-Simpson scale in same units as wind (default scale valid for knots).

        Returns
        -------
        category : int
            Intensity of given track according to the Saffir-Simpson hurricane scale:
            * -1 : tropical depression
            *  0 : tropical storm
            *  1 : Hurricane category 1
            *  2 : Hurricane category 2
            *  3 : Hurricane category 3
            *  4 : Hurricane category 4
            *  5 : Hurricane category 5
        """
      
        if saffir_scale is None:
            saffir_scale = SAFFIR_MS_CAT
       
        max_wind = np.nanmax(max_sus_wind)
        try:
            return (np.argwhere(max_wind < saffir_scale) - 1)[0][0]
        except IndexError:
            return -1
        
    @staticmethod
    def check_last_forecast( ref_date:Union[str, date]=date.today(), days_back:int=0,
                             model_times:Union[str, List]=None,
                             pattern:str='tropical_cyclone_track',
                             stop_first_occurence:bool=True  ):
            """
            Get the list of the last  ECMWF tropycal cyclones forecasts
            """
            logger.info("")
            if not isinstance(ref_date, date):
                ref_date = parse(ref_date).date()
            logger.info (f"Ref date = {ref_date}")


            parsed_url = urlparse(ECMWF_URL_BASE)
       
            fs = fsspec.filesystem(protocol=parsed_url.scheme )

            pattern = re.compile(f".+{pattern}.+\.bin")
            
            # build the list of days starting from the ref_date and going back in time
            days = [ (ref_date - timedelta(days=d)).strftime("%Y%m%d") for d in  range(days_back+1) ] 
        
            logger.info (f"Checking for ECMWF model forecast over the last {days_back} days :  {days}")

            # zip together days and forecast times
            if model_times == None:
                model_times = MODEL_TIMES[::-1]
                forecast_dates = list(  map(''.join, itertools.product(days, model_times)))
            else:
                assert model_times in MODEL_TIMES, f"Error model_time shpuld be one of {MODEL_TIMES}"
                forecast_dates = [f"{day}{model_times}" for day in days]
            
            url_list = []
            for fct_date in forecast_dates:
                
                url_ = urlparse(f"{ECMWF_URL_BASE}/{fct_date}/")
              
                logger.info (f"Checking for {fct_date}")
                # fsspec is sensitive to double slashes
                safe_path = url_.path.replace('//', '/')
                url_ = urlunparse((url_.scheme, url_.netloc, safe_path, url_.params, url_.query, url_.fragment))
                # perform a remote ls on the remote http directory
                try:
                    file_list = fs.ls(url_, username=ECMWF_USER, password=ECMWF_PASSWORD, detail=False)                
                except:
                    file_list=[]
                    
                file_list2 = [ f for f in file_list  if re.match(pattern, f) ]
                
                url_list.append(file_list2)
                logger.debug (f"Found {url_list}")
                   
                if (stop_first_occurence == True) & (len(file_list2) > 0):
                    logger.info (f"Found {len(file_list2)} occurences on {fct_date}: stopping loops ")
                    break
            #flatten the list
            flat_list = list(itertools.chain(*url_list))
            logger.info (f"Total number of files found {len(flat_list)}  ")
            return flat_list 

        