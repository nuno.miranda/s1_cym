from typing import Union
import pandas as pd
import geopandas as gpd
from lxml import etree
import numpy as np
from scipy import constants
from shapely.geometry import Point, MultiPoint, LineString
from geographiclib.geodesic import Geodesic
import simplekml 

from dateutil.parser import parse
from datetime import datetime,timedelta
from pathlib import Path
import re
from . import data 
import matplotlib.pyplot as plt

from .tc_track import  TCTracks, CAT_NAMES     
from .logger._logger import get_logger
logger = get_logger(__name__)

IBTRACS_3Y="https://www.ncei.noaa.gov/data/international-best-track-archive-for-climate-stewardship-ibtracs/v04r00/access/shapefile/IBTrACS.last3years.list.v04r00.lines.zip"
IBTRACS_ACTIVE="https://www.ncei.noaa.gov/data/international-best-track-archive-for-climate-stewardship-ibtracs/v04r00/access/shapefile/IBTrACS.ACTIVE.list.v04r00.lines.zip"

CAT_NAMES_COLOR = dict( zip(CAT_NAMES.keys(),  ['ff5dbaff', 'ff00fbf4', 'ffffffcd','fffee775', 'ffffc140',	'ffff8f21','ffff6060']))

class HurricaneTrajectory:
    """HurricaneTrajectory

    Basic class hosting a cyclone trajectory.

    Parameters
    ----------
    source : str 
        Source of the trajectory e.g. CLS, NOAA, ECMWF
    name: str
        name of the Hurricane
    waypoints_time: datetime
        list of time for each trajectory point

    waypoints: list(Point):
        list of waypoint stored in shapely.Point
    

    crs: str
        Coordinate Reference System string

    Attributes
    ----------
    df : GeoDataFrame
        containing the provided elements
    """

    def __init__(self, source, name, waypoints_time, waypoints,  wind_speed=None,crs='EPSG:4326'):
        self.name =name
        self.source = source
        self.time = waypoints_time
        self.waypoint = waypoints

        df = pd.DataFrame.from_dict({'TIME':waypoints_time, }) 
        self.df = gpd.GeoDataFrame(df, geometry=waypoints, crs=crs)
        self.df['NAME'] = name
        self.df['SOURCE'] = source
        
        self.df['WIND_SPEED']=wind_speed
        self.df['CATEGORY']=(self.df['WIND_SPEED']*1.94384449).map(TCTracks.set_category)
        self.df['CAT_NAME']=self.df['CATEGORY'].map(CAT_NAMES)

        self.df.sort_values(by=['TIME'],  ignore_index=True, inplace=True)
        self.df['TIME_FROM_FORECAST'] = (self.df['TIME'] -  self.df['TIME'].loc[0]).astype('timedelta64[s]')  / 60/60
        self.df['TIME_FROM_FORECAST'] = self.df['TIME_FROM_FORECAST'].map(lambda x: pd.Timedelta(x,  unit="h"))
 
    
    def write(self, trajectory_filename, hours_from_forecast=None):
        if hours_from_forecast== None:
            hours_from_forecast= self.df.TIME_FROM_FORECAST.max()
        else:
            hours_from_forecast = pd.Timedelta(hours_from_forecast,  unit="h")
        
        df = self.df [self.df.TIME_FROM_FORECAST <= hours_from_forecast]
        sum=0
        logger.info(f"Create trajectory {trajectory_filename.name} in folder {trajectory_filename.parent}")
        with open(trajectory_filename, 'w') as f:
            for ix, pt in df.iterrows():
                #if self.__is_land(pt.geometry) == False:
                sum+=1
                t =  self.to_mjd2000(pt.TIME)
                f.write(f"{t} {pt.geometry.x} {pt.geometry.y} 0.0\n")
        logger.info(f"Writing track points: {sum} / {len(df)}")

    def plot(self,legend=False, start=None, stop=None, world=True, ax=None,annotate=False, margin=1, **kw_plot):
        
        if ax == None:
            fig, ax = plt.subplots(ncols=1, nrows=1, figsize = (6, 6))
        else:
            xlim = ax.get_xlim()
            ylim = ax.get_xlim()
            
       
        if world:
            world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
            world.plot(ax=ax, edgecolor='black', linewidth=0.5, column='continent', alpha=0.5, cmap='gist_yarg')
            ax.set_facecolor("lightblue")

        if start == None: start = self.df.TIME.iloc[0]
        if stop == None: stop = self.df.TIME.iloc[-1]
        
        df_ = self.df[  self.df.TIME.between( start, stop) ]
        ls_ = LineString(df_.geometry.values)

        msize = df_['TIME_FROM_FORECAST'].dt.total_seconds() /60/60
        ax=df_.plot(column='CAT_NAME', markersize=msize, ax=ax, legend=legend,  **kw_plot )
        #
        ax.plot(ls_.xy[0].tolist(), ls_.xy[1].tolist(), color='gray')

        if annotate:        
            for ii, an in df_.iloc[[0,-1]].iterrows():
                sign = (-1)**ii
                txt = parse(an.TIME.isoformat()).strftime("%d/%b")
                #print(an)
                ax.annotate(txt, xy=(an.geometry.x,an.geometry.y), xycoords='data',size=8,
                    xytext=(0, sign*20), textcoords='offset points',
                    arrowprops=dict(arrowstyle="-")) 
        
        bds = df_.total_bounds
        mm = np.asarray([-1,1]) * margin
        ax.set_xlim(bds[0::2] + mm)
        ax.set_ylim(bds[1::2] + mm)
        
        if legend:
            leg = ax.get_legend()
            leg.set_bbox_to_anchor((1.1, 1.05))
        return ax

    @classmethod
    def from_noaa_ibtracs(cls, cyclone_name, df=None, active:bool=False):
        """Parsing the trajectory file from the NOAA IBTRACS database

        Parameters
        ----------
        cyclone_name : str
            Name of cyclone to look for in the IBTRACS record
        active : bool
            if the boolean is set to True the only the active cyclones will be tracked 

        """
        logger.info(f"Parsing trajectory from IBTRACS")
        IBTRACS = IBTRACS_3Y
        if active : 
            IBTRACS = IBTRACS_ACTIVE
            logger.info("Reading only the active Hurricanes")
        
        if  not isinstance(df, pd.DataFrame):
            df = gpd.read_file(IBTRACS,)# header=0, parse_dates=[5],infer_datetime_format=True)
        
        tc = df[ df['NAME'].isin([cyclone_name])]

        logger.info(f"Found track with {len(tc)} points")
        times = [ parse(tt) for tt in tc.ISO_TIME.values ]

        coords = [ (rec.LON, rec.LAT) for ii , rec in tc.iterrows()]
        
        #wrap anti-meridian
        logger.info(f"Wrapping coordinates around the anti-meridian")
        coords = np.asarray(coords, dtype=float)
        ix = coords [:,0] > 180
        coords[ix,0]-=360
        
        coords = [ Point(np.asarray(coord).astype(float)) for coord in coords]

        return cls('IBTRACS',cyclone_name, times, coords)

    @classmethod
    def from_ecmwf_bufr(cls, bufr:str):
        """Parsing the trajectory file in bufr as provided by ECMWF and instanciate a class

        Parameters
        ----------
        bufr : str
            The name of tke bufr file to load


        """

        tc_track = TCTracks.from_ecmwf_bufr(bufr)

        if tc_track != None:
            coords = [ (lon, lat) for lon, lat in zip(tc_track[0].lon, tc_track[0].lat)]
            #wrap anti-meridian
            logger.info(f"Wrapping coordinates around the anti-meridian")
            coords = np.asarray(coords, dtype=float)
            ix = coords [:,0] > 180
            coords[ix,0]-=360
            
            coords = [ Point(np.asarray(coord).astype(float)) for coord in coords]

            
            return cls('ECMWF',tc_track[0].name, tc_track[0].time, coords, wind_speed=tc_track[0].max_sustained_wind)
        else:
            return None
            

    @classmethod
    def from_cls_kml(cls, kml_track:str):
        """Parsing the trajectory file in KML as provided by CLS and instanciate a class

        Parameters
        ----------
        kml_track : str
            The name of tke KML file to load
        """

        logger.info(f"Parsing trajectory file: {kml_track}")
        root = etree.parse(kml_track).getroot()
        
        #find the Hurricane name and cleanup
        xp = 'Document/name'
        name = root.find(xp, root.nsmap).text.replace(" ", "")
        name = re.sub(r'[()]','_' ,name)
        logger.info (f"Hurricane Name : {name}") 

        # getting date and parsing as datetime
        xp ="Document/Folder[@id='Forecasted Centres']/Placemark/TimeStamp/when"
        times = [parse(rec.text) for rec in root.findall(xp, root.nsmap) ]

        #todo : getting wind speed velocity if available
            
        # getting coordiantes and load as shaply.Point
        xp ="Document/Folder[@id='Forecasted Centres']/Placemark/Point/coordinates"
        coords = [(rec.text.split(',')) for rec in root.findall(xp, root.nsmap) ]
        coords = np.asarray(coords, dtype=float)
        logger.info(f"Wrapping coordinates around the anti-meridian")
        ix = coords [:,0] > 180
        
        coords[ix,0]-=360
        coords = [ Point(np.asarray(coord).astype(float)) for coord in coords]

        return cls('CLS', name, times, coords, crs='EPSG:4326') 
    
    def interpolate(self, time=None):
        """
        Interpolate the trajectory
        """

        logger.info(f"Interpolating trajectory {self.name} : {len(self.df)}")

        # resample the trajactory time at 1h interval
        if time == None:
            time = self.df.resample('60min', on='TIME').count().index.values
        #print (self.df.TIME.iloc[0], self.df.TIME.iloc[-1],)
        pti = []
        for ii, tt in enumerate(time):
            
            # get the two rows bracketimg the time requested
            dd_before = self.df[  self.df.TIME <= tt  ]
            dd_after = self.df[  self.df.TIME > tt  ]
            if len(dd_after) == 0:
                dd_after =self.df[  self.df.TIME >= tt  ]

            dd = self.df[  self.df.TIME.between(  tt -pd.Timedelta(hours=6), tt + pd.Timedelta(hours=6)) ]

            point1 = dd_before.iloc[-1]
            point2 = dd_after.iloc[0]
            # time separation between the 2 known points
            ts1 = (tt - point1.TIME).total_seconds()
            ts2 = (tt - point2.TIME).total_seconds()
            #print (f" {ii} / {len(time)} {dd_before.iloc[-1].TIME} {tt} {dd_after.iloc[0].TIME} {ts1}, {ts2}")
            point3 = self._interpolate(point1.geometry, point2.geometry, point1.TIME, point2.TIME, tt)
            pti.append(point3)
        #logger.info(f"Interpolate {time_1h.size} {len(pti)}")
        
        if self.df.WIND_SPEED.isna().any() ==False:
            logger.info("interpolating wind speeds")
            ti = time.astype('datetime64[h]').astype(int)
            t_ = self.df.TIME.values.astype('datetime64[h]').astype(int)
            wsi = np.interp(ti,t_ , self.df.WIND_SPEED.values)
        
        new_traj=HurricaneTrajectory(self.source, self.name, time,pti, wind_speed=wsi )
        return new_traj



  
    @staticmethod
    def _interpolate(point1:Point, point2:Point, time1, time2, time):
        """
        Interpolate a route between two set of latitude and longitues
        """
        lon1, lat1 = point1.x, point1.y
        lon2, lat2 = point2.x, point2.y
        #print (point1, point2)
        
        # Get distance and direction between initial points
        inverse_result = Geodesic.WGS84.Inverse(lat1, lon1, lat2, lon2)
        distance = inverse_result['s12']
        angle = inverse_result['azi1']
        
        # Calculate new distance from the first point
    
        dt12 = ((time2 - time1).total_seconds())
        dt13 = ((time - time1).total_seconds())
        if dt12 == 0:
            return point1
        average_speed = distance / dt12
        distance3 = average_speed * dt13
        
        # Get new location
        direct_result = Geodesic.WGS84.Direct(lat1, lon1, angle, distance3)
        lat3 = direct_result['lat2']
        lon3 = direct_result['lon2']

        return Point (lon3, lat3)


    def error_vs_ibtracs(self, best_track=None):
        geod = Geodesic.WGS84 
        if best_track == None:
            best_track = self.from_noaa_ibtracs(self.name)

        time_ref = self.df.iloc[0].TIME
        distance = []
        time = []
        ts = []
        geom =[]
        for ix, rec in self.df.iterrows():
            lon1, lat1 = rec.geometry.x, rec.geometry.y
    
            df = best_track.df [best_track.df.TIME == rec.TIME]
            #print (df)
            if len(df) >0:
                dt =  df.iloc[0].TIME - time_ref

               
                lon2, lat2 = df.iloc[0].geometry.x, df.iloc[0].geometry.y
                g = geod.Inverse(lat1, lon1, lat2, lon2)
                
                distance_3d = np.hypot(g['s12'],0)
                
                ts.append(dt)
                distance.append(distance_3d)
                time.append(df.iloc[0].TIME)
                geom.append(df.iloc[0].geometry) 
                print(f" {rec.TIME} : ({lon1}, {lat1}) ({lon2},{lat2})  {dt.total_seconds()/60/60/24:05.5f} : {distance_3d/1000} km,")

        dd = {'TIME': time, 'TIME_DIFF':ts, 'DISTANCE':distance, 'geometry':geom}

        return pd.DataFrame.from_dict(dd)
        
    def to_kml(self, kml:simplekml.Kml=None, write=True, subset=True):
        hours = list(range (0,24,1))
        if subset:
            hours = list(range (0,24,6))
            
        if kml == None:
            kml_doc = simplekml.Kml()
        
        kml_dir = kml_doc.newfolder(name=f"Hurricane Track")
        
        ls = kml_dir.newlinestring(name='trackline')
        ls.coords = list(LineString(self.df.geometry.values).coords)
        ls.extrude = 1
        ls.style.linestyle.width = 2
        ls.style.linestyle.color = simplekml.Color.white
        
        kml_dir2 = kml_dir.newfolder(name=f"trackpoint")
        for ii, rec in self.df.iterrows():
            ts = rec.TIME
            if ts.hour in hours:
                pnt = kml_dir2.newpoint(name=f'{ts.strftime("%d/%b %H:%M")}')
                pnt.coords = [(rec.geometry.x, rec.geometry.y) ]
                pnt.style.labelstyle.scale = 0.8  # Make the text twice as big
                t0, t1 = rec.TIME - timedelta(minutes=30), rec.TIME + timedelta(minutes=30)
                pnt.timespan.begin = t0.isoformat()[0:13]
                pnt.timespan.end = t1.isoformat()[0:13]
                pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png'
                pnt.style.iconstyle.color = CAT_NAMES_COLOR[rec.CATEGORY]

        fname = f"{self.name}_track.kml"
        if write:
            logger.info(f"Write {fname}")
            kml_doc.save(fname)
        
        return kml_doc

    @staticmethod
    def to_mjd2000(timestamp, ref=np.datetime64('2000-01-01T00:00:00Z'), to_days:bool=False):
        """
        Convert a timestapm into elapsed seconds since an reference date
        """
        if hasattr(timestamp, '__iter__') == True:
            timestamp = np.asarray(timestamp)
            if isinstance(timestamp[0] , datetime):
                ref =  datetime(2000,1,1)
                time = [(t_-ref).total_seconds() for t_ in  timestamp]
                time = np.asarray(time)
            elif isinstance(timestamp[0] , np.datetime64):
                time = (timestamp - ref).astype('timedelta64[ns]')
                time = (np.double(time)* constants.nano).reshape(timestamp.shape)

        else:
            if isinstance(timestamp , datetime):
                ref = datetime(2000, 1, 1)
                time = (timestamp-ref).total_seconds()
            elif isinstance(timestamp , np.datetime64):
                time = (timestamp - ref).astype('timedelta64[ns]')
                time = np.double(time)* constants.nano

        if to_days:
            time /= constants.day
        return time