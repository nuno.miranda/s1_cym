import re
from pathlib import Path
import requests
from bs4 import BeautifulSoup
from .logger._logger import get_logger

logger = get_logger(__name__)

CFI_URL_BASE = 'https://eop-cfi.esa.int'

SWATH_TO_SDF ={'EW1': 'SDF_SAR1EW.S1',
'EW1': 'SDF_SAR1IW.S1',
'S1': 'SDF_SAR1SM.S1',
'WV1': 'SDF_SAR1WV.S1',
'EW2': 'SDF_SAR2EW.S1',
'IW2':'SDF_SAR2IW.S1',
'S2': 'SDF_SAR2SM.S1',
'WV2': 'SDF_SAR2WV.S1',
'EW3':'SDF_SAR3EW.S1',
'IW3':'SDF_SAR3IW.S1',
'S3':'SDF_SAR3SM.S1',
'EW4':'SDF_SAR4EW.S1',
'S4':'SDF_SAR4SM.S1',
'EW5':'SDF_SAR5EW.S1',
'S5':'SDF_SAR5SM.S1',
'S6':'SDF_SAR6SM.S1',
'EW':'SDF_SARWEW.S1',
'IW':'SDF_SARWIW.S1'}

class CfiDataRepo:
    def __init__(self,  url_base='https://eop-cfi.esa.int'):
        self.session = requests.Session()
        self.session.verify = False
        self.url_base = url_base
    
    def get_sdf(self, mission:str, swath:str, outdir='./', url_ext='/Repo/PUBLIC/DOCUMENTATION/MISSION_DATA/ORBIT_SWATH_DATA/'):
        """Get Swath definition file"""
        
        unit = mission[2]
        logger.info(f"Downloading {SWATH_TO_SDF[swath]}...") 
        assert (unit in ['A', 'B', 'C', 'D'])
        url_ = f"{self.url_base}/{url_ext}/SENTINEL1{unit}/SDF/{SWATH_TO_SDF[swath]}"
        r = self.session.get(url_)
        outfile = Path(outdir)/ SWATH_TO_SDF[swath]
        with open(outfile, 'wb') as f:
            logger.info(f"Write to {outfile.absolute()}...") 
            f.write(r.content)
            return outfile
    
    def get_osf(self, mission:str, outdir='./', url_ext='/Repo/PUBLIC/DOCUMENTATION/MISSION_DATA/ORBIT_SWATH_DATA/'):
        """Get Swath definition file"""
  
        pattern = (".+\.EOF")
        unit = mission[2]
        logger.info(f"Downloading orbit scenario file for {mission}...") 
        assert (unit in ['A', 'B', 'C', 'D'])

        url_ = f"{self.url_base}/{url_ext}/SENTINEL1{unit}/OSF/"
        logger.info(f"Downloading from  {url_}...") 
        page = requests.get(url_, verify=False, allow_redirects=True)
        if page.status_code == 200:
            soup = BeautifulSoup(page.content, 'html.parser')
            for a in soup.find_all('a'):
                href = a.attrs.get('href')
                print (href)
                if re.match(pattern, href):
                    sct = Path(href).name
                    print (sct)
            
            