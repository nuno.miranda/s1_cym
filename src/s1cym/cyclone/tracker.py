
from importlib import resources
from dataclasses import dataclass
import os
from datetime import timedelta
from dateutil.parser import parse
import re
from pathlib import Path
import subprocess
import shutil

import simplekml 
from lxml import etree
import pandas as pd
import geopandas as gpd

from shapely.geometry import LineString, Point
from polycircles import polycircles

from .tc_track import  TCTracks, CAT_NAMES 
from . import data 
from .utils import pass_generator, heading
from .logger._logger import get_logger
from .trajectory import HurricaneTrajectory , CAT_NAMES_COLOR 
  
logger = get_logger(__name__)

# SWATH DEFINITION FILE
S1_SDF = {'IW' : "SDF_SARWEW.S1", "EW" : "SDF_SARWEW.S1"  }
S1_SWATH_EXTENT = {'IW' : 250, "EW" : 420  }



# TODO: to update with other S-1 mission or to find a way to get the data automatically from the site
S1_ORBSCT={"S1A" : "S1A_OPER_MPL_ORBSCT_20140507T150704_99999999T999999_0024.EOF",
           "S1B" : "S1B_OPER_MPL_ORBSCT_20160425T224606_99999999T999999_0025.EOF"}


class CycloneTracker:
    """CycloneTracker

    Basic class for propagating S-1 following a hurricane trajectory.
    The class will return a hint if there is a S-1 overpass intersecting a circle around the trajectory points
    within a maximum time separation.

    The circle radius is the radius class parameter
    The maximum time separation is the max_time_diff

    Parameters
    ----------
    mission : str 
        mission name S1A, S1B, S1C , S1D
    name: str
        S-1 instrument mode (EW, IW)
    
    trajectory: HurricaneTrajectory
        Hurricane trajectory class

    orbit_scenario_file: str
        Orbit scenario file to use.
        If not provided a default one will be used
    
    radius: float
        Radius of search circle arounf the trajectory point in meters
    
    max_tca_diff: float
        The maximum time separation in fraction of day between the Time of Closest Approach (SAR ) and the Hurricane track point
    Attributes
    ----------
    df : GeoDataFrame
        containing the provided elements
    """    
    def __init__(self, mission:str, mode:str, trajectory:HurricaneTrajectory, orbit_scenario_file:str=None, radius=100000, max_tca_diff=0.08):
        self.mission = mission
        self.mode = mode
        self.max_radius = radius
        self.max_tca_diff = max_tca_diff
        
        with resources.path(data, S1_SDF[mode]) as sdf:
            logger.info (f"Selecting SDF from {mission} {mode} : {sdf}")  
            self.sdf = sdf
        
        if orbit_scenario_file == None:
            with resources.path(data, S1_ORBSCT[mission]) as s1_orb:
                self.orbit = s1_orb
        else:
            self.orbit = orbit_scenario_file
        logger.info (f"Selecting orbit scenario file {self.orbit }")  

        self.track=trajectory

        world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
        world = world[['continent', 'geometry']]

        self.world = world.dissolve()

    @property
    def name(self):
        return self.track.name
    
    def trajectory_filename(self, wdir='./'):
        f = Path(wdir) / f"{self.name}_trajectory.txt"
        return f

    def config_filename(self, wdir='./'):
        f = Path(wdir) /  f"{self.mission}_{self.mode}_{self.name}__config.txt"
        return f


    def write(self, config_filename, trajectory_file,  new_dir=None, radius=100000, max_tca_diff=0.08):
        """
        Writes a configuration file and a trajectory file to disk. The configuration file contains the following fields:
                - Satellite ID: allowed values areAEOLUS, CRYOSAT2, EARTHCARE, METOPSG, SENTINEL1A, SENTINEL1B, SENTINEL2A, SENTINEL2B, SENTINEL3A, SENTINEL3B, SENTINEL5P, SENTINEL6, SMOS or GENERIC)
                - Orbit Filename:  MPL_ORBSCT, for AEOLUS, AE_OPER_MPL_ORBSCT_20180822T225352_99999999T999999_0015.EEF
                - Swath Filename: swath definition file
                - Cyclon Trajectory Filename (file with list of balloon points)
                - Instrument Swath ID (any string, e.g. ALADIN)
                - Swath Color (aabbggrr, aa=alpha, bb=blue, gg=green rr=red, with ranges 00 to ff)
                - Radius of the circle around balloon point in meters. Typical value: 100000.0
                - Reference time of the balloon data. Expected calendar CCSDS format: YYYY-MM-DDTHH:MM:SS
                - Maximum time difference between balloon timestamp and observation time, in decimal days
                - Balloon Filetype (allowed strings: SEC_SINCE_REF_DATE_LON_LAT_H or SEC_SINCE_REF_DATE_LON_LAT_H_WIND_U_V). If empty string, by default SEC_SINCE_REF_DATE_LON_LAT_H is used.
        

        Parameters
        ----------
        config_filename : str
            Name of the configuration file to write.
        trajectory_file : str
            Name of the trajectory file to write.
        new_dir : str, optional
            If specified, the configuration and trajectory files will be written to the given directory.
            Otherwise, they will be written to the current working directory.
        radius : int, optional
            The radius (in meters) around the reference point to include in the trajectory file.
            Default is 100000.
        max_tca_diff : float, optional
            The maximum TCA difference (in fraction of days) between two points in the trajectory.
            Default is 0.08 days.

        Returns
        -------
        None
        """

        if new_dir != None:
            logger.info(f"Replacing path in {new_dir}")
            orbit_file = Path(new_dir) / self.orbit.name
            sdf_file = Path(new_dir) / self.sdf.name
        else:
            orbit_file =  self.orbit.name
        sdf_file =  self.sdf.name 


        with open(config_filename, 'w',) as f:
            dd = {'S1A' : 'SENTINEL1A', 'S1B' : 'SENTINEL1B'}

            f.write(f"{dd[self.mission]}\n")
            f.write(f"{orbit_file}\n")
            f.write(f"{sdf_file}\n")

            f.write(f"{trajectory_file}\n")
            f.write(f"{self.mode}\n")
            f.write(f"990000ff\n") # color
            f.write(f"{radius}\n") # radius
            f.write("2000-01-01T00:00:00\n")
            f.write(f"{max_tca_diff}\n")
            f.write(f"SEC_SINCE_REF_DATE_LON_LAT_H\n")
            
    def create_wdir(self, wdir_name:str=None, max_forecast_age:float=None):
        """
        Creates a working directory with the specified name and returns a message indicating success.

        Parameters
        ----------
        wdir_name : str, optional
            The name of the working directory to be created. Default is None.
        max_forecast_age : float, optional
            limit the trajectory file to a track point not older than max_forecast_age.
            if not set, then all track points are used

        Returns
        -------
        str, str, str
            The namw of wdir and files written.

        """
    
        if wdir_name == None:
            wdir_name =f"{self.name}_{self.track.df.TIME.loc[0].isoformat()[0:13]}"
        wdir_name = Path(wdir_name)
        
        logger.info(f"create WDIR : {wdir_name}")
        if not wdir_name.exists() : 
            wdir_name.mkdir(exist_ok=True)

        trajectory = self.trajectory_filename(wdir=wdir_name)
        config =  self.config_filename(wdir=wdir_name)

        # write on disk the trajectory file
        self.track.write(trajectory, hours_from_forecast=max_forecast_age)

        # write the config on disk
        self.write(config,trajectory.name,radius=self.max_radius, max_tca_diff=self.max_tca_diff )

        for f_ in [self.orbit, self.sdf]:
            f_new = wdir_name / f_.name
            shutil.copy(f_, f_new)
        return wdir_name, trajectory, config
    
    @staticmethod
    def pack_wdir(wdir:str):
        """
            Packs a given directory path into a compressed archive format.

            Parameters:
            -----------
            wdir : str
                A string representing the path to the directory to be packed.

            Returns:
            --------
            str 
                Returns the name of the zip file or None
            """
        wdir = Path(wdir)
        if wdir.exists():
            shutil.make_archive(str(wdir), 'zip')
            return wdir.with_suffix('.zip')
        else:
            return None



    def _read_csv_result(self, csv_filename):
        df_=pd.read_csv(csv_filename, skiprows=14, skipinitialspace=True, parse_dates=['BALLOON_UTC_TIME', 'UTC_TIME'])
        df_.drop(df_.index[-1], inplace=True)
        df_['NAME'] = self.track.name
        return(df_)

    @staticmethod
    def _get_cfi_tracker():
        """
        Function to get the path to the CYCLONE_TRACKER_CFI env variable
        """
        
        # check if the environment variable is set
        cfi_tracker = os.getenv('CYCLONE_TRACKER_CFI')
        if cfi_tracker == None:
            # if not trying sourcing the bash_profile
            logger.info(f"Environment variable CYCLONE_TRACKER_CFI is not available ... trying by sourcing the ~/.bash_profile")
            cmd_str = f"source  ~/.bash_profile && echo $CYCLONE_TRACKER_CFI"
            res = subprocess.run(cmd_str, shell=True,capture_output=True, text=True,  executable='/usr/bin/bash')
            
            if res.stdout != None:
                cfi_tracker=  res.stdout.rstrip().split("\n")[-1]
                logger.info(f"Environment variable CYCLONE_TRACKER_CFI found {cfi_tracker}")
            else:
                logger.error(f"Environment variable CYCLONE_TRACKER_CFI not found ")
        return cfi_tracker
        

    def to_kml(self, kml_doc:simplekml.Kml=None):

        if kml_doc == None:
            kml_doc = simplekml.Kml()
            if self.wdir.exists():
                # write the Hurricane Track in a Folder
                kml_doc = self.track.to_kml(write=False)
  
        
        if 'GOOD' not in self.df.columns:
            self.df.define_quality()
        
        tracker_df = self.df.set_index('BALLOON_ID')
        
        if self.wdir.exists():
            # create two folder OK, NOT OK
            kml_ok =  kml_doc.newfolder(name=f"{self.mission}_{self.mode}_GOOD")
            kml_nok =  kml_doc.newfolder(name=f"{self.mission}_{self.mode}_REJECTED")
            #parse te KML create by the tracker
            df = self.parse_kml_balloon()
            
            #loop on the closest approach found
            for ix, rec in df.iterrows():
                rec2 = tracker_df.loc[rec.balloon_id]
                is_good = rec2.GOOD == "*"  
                fld = kml_ok if is_good else kml_nok
                
                kml_fld = fld.newfolder(name=f"TCA_{rec2.MISSION}_{rec2.MODE}_{rec.TCA}")

                # from the swath line create a pass and associate a tme span to it
                s1_pass =  pass_generator(rec.sar_geometry, velocity=7000, duration=30)  
                t0, t1 = rec.TCA - timedelta(minutes=30), rec.TCA + timedelta(minutes=30)
            
                # create the rectangle associated to the pass
                s1_pass_kml = kml_fld.newpolygon(name=rec.sar_id)
                s1_pass_kml.timespan.begin = t0.isoformat()[0:13]
                s1_pass_kml.timespan.end = t1.isoformat()[0:13]
                s1_pass_kml.outerboundaryis =  list(s1_pass.exterior.coords)
                s1_pass_kml.extrude = 1
                s1_pass_kml.style.linestyle.width = 2
                s1_pass_kml.style.linestyle.color = "99ffcc99"
                if is_good:
                    s1_pass_kml.style.polystyle.color = simplekml.Color.changealphaint(100, simplekml.Color.green) 

                else:
                    s1_pass_kml.style.polystyle.color = simplekml.Color.changealphaint(100, simplekml.Color.red)
                
                # create a pin point for each closest approach
                pnt = kml_fld.newpoint(name=f'{rec.balloon_id}')
                pnt.coords = [(rec.balloon_geometry.x, rec.balloon_geometry.y) ]
                pnt.style.labelstyle.scale = 0.8  # Make the text twice as big
                pnt.style.iconstyle.color = CAT_NAMES_COLOR[rec2.CATEGORY]
                pnt.timespan.begin = t0.isoformat()[0:13]
                pnt.timespan.end = t1.isoformat()[0:13]
                
                # create a circle centered on the Hurricane track point
                polycircle = polycircles.Polycircle(latitude=rec.balloon_geometry.y,
                                    longitude=rec.balloon_geometry.x,
                                    radius=200e3,
                                    number_of_vertices=26)
                
                pol = kml_fld.newpolygon(name="Cyclone Error Circle", outerboundaryis=polycircle.to_kml())
                pol.style.polystyle.color = simplekml.Color.changealphaint(100, simplekml.Color.yellow)
                pol.timespan.begin = t0.isoformat()[0:13]
                pol.timespan.end = t1.isoformat()[0:13]

                        
                kml_fld.extendeddata.newdata(name='closest_approach', value=str(rec.TCA))
                kml_fld.extendeddata.newdata(name='mission', value=rec2.MISSION)
                kml_fld.extendeddata.newdata(name='mode', value=rec2.MODE)
                kml_fld.extendeddata.newdata(name='orbit_number', value=rec2.ABS_ORBIT,)
                kml_fld.extendeddata.newdata(name='track_number', value=rec2.REL_ORBIT)
                kml_fld.extendeddata.newdata(name='distance_mid_swath', value=rec2['DISTANCE_TO_MID_SWATH[km]'])
                kml_fld.extendeddata.newdata(name='forecast_Age', value=rec2['FORECAST_AGE'])

                
                fname = self.wdir / f"{self.mode}_{self.name}_tracker.kmz"
            logger.info(f"Write {fname}")
            kml_doc.savekmz(fname, format=False)
        else:
            logger.info(f"CANNOT Write  KMZ{fname}")
        return fname, kml_doc

        

    def __call__(self, wdir=None, max_forecast_age=None):
        wdir, trajectory, config = self.create_wdir(wdir_name=wdir, max_forecast_age=max_forecast_age)
        
        self.wdir = wdir
        self.trajectory = trajectory
        self.config = config
        logger.info(f"Getting CYCLONE_TRACKER_CFI")
        cfi_tracker = self._get_cfi_tracker()
        
        if cfi_tracker != None:
            cfi_tracker = Path(cfi_tracker)
            #logger.info (f"Found EOCFI tracker {cfi_tracker}")  
          
            cmd_str = f"{cfi_tracker} {config.name} FILE_RANGE "
            logger.info(f"RUN : {cmd_str}")
            
            res=subprocess.run(cmd_str, shell=True,cwd=wdir, capture_output=True, text=True,  executable='/usr/bin/bash')
            dd =  self.track.df.set_index('TIME')
            if res.returncode == 0:
                csv_file = list(wdir.glob(f"{self.mission}_{self.mode}*.CSV"))[0]
                
                self.df= self._read_csv_result(csv_file)

                #clean up the file
                self.df['MISSION'] = self.mission
                self.df['MODE'] = self.mode
                self.df.rename(columns={ "BALLOON_UTC_TIME" : "CYC_UTC_TIME", "UTC_TIME" : "SAR_UTC_TIME"}, inplace=True)
                self.df ['TIME_DIFF_VS_TRACK[MIN]'] = self.df ['TIME_DIFFERENCE_OBS_VS_BALLOON[days]'] * 24 * 60

                self.df['FORECAST_AGE'] = (self.df.CYC_UTC_TIME -self.track.df.TIME.min()).dt.total_seconds() /(24 * 60 *60  )

                # associate the category directly
                self.df ['CAT_NAME'] = dd.loc[self.df.CYC_UTC_TIME].CAT_NAME.values
                self.df ['CATEGORY'] = dd.loc[self.df.CYC_UTC_TIME].CATEGORY.values
                
                #
                self.land_flag()   
                

                # Montse tool has a small error in calculating the distance to mid swath
                self._recompute_closest_approach_distance()
                logger.info (f"Found {len(self.df)} possible intersections with {self.mission}_{self.mode}")
                #logger.info (f"\n{self.df[['SAR_UTC_TIME', 'DISTANCE_TO_MID_SWATH[km]', 'TIME_DIFF_VS_TRACK[MIN]']]}")
                return self.df

        else:
            logger.error(f" EOCFI tracker {cfi_tracker} NOT FOUND!")  
            self.df = pd.DataFrame()
            return self.df

    
    def _recompute_closest_approach_distance(self):
        #parse the KML to get the swath linestring
        kml_df = self.parse_kml_balloon()
        if not kml_df.empty:
            #set the index to be the ballon_id
            kml_df.set_index('balloon_id', inplace=True)
            self.df.set_index('BALLOON_ID', inplace=True)
            dd= []
            for balloon_ix, rec in kml_df.iterrows():
                mid_swath = rec.sar_geometry.centroid
                balloon = rec.balloon_geometry

                hd, dist = heading(balloon, mid_swath)
            
                self.df.loc[balloon_ix,'DISTANCE_TO_MID_SWATH[km]' ] = dist / 1000

            self.df.reset_index(inplace=True)

    def land_flag(self):
        landflag = []
        for ix, rec in self.df.iterrows():
            pt = Point(rec['BALLOON_LON[deg]'],	rec['BALLOON_LAT[deg]'])
            landflag.append(self.__is_land(pt))
        
        self.df['OVER_SEA'] = not landflag
                       
    def __is_land(self, geometry):
        return self.world.intersects(geometry).values[0]
    
    def define_quality(self, max_distance:float=None, max_forecast_age:float=4, max_days:float=1, tc_category=0,columns=None):
        if columns == None:
            columns = [ 'GOOD', 'CAT_NAME', 'CATEGORY',  'NAME', 'MISSION', 'MODE', 'ABS_ORBIT', 'REL_ORBIT',
                       'SAR_UTC_TIME', 'CYC_UTC_TIME', 'DISTANCE_TO_MID_SWATH[km]', 'TIME_DIFF_VS_TRACK[MIN]',
                        'FORECAST_AGE', 'OVER_SEA']
        if not self.df.empty:
            self.df['GOOD']=0
            if max_distance == None:
                max_distance = S1_SWATH_EXTENT[self.mode] / 2
            ix0 = self._check_closest_approach_distance(max_distance=max_distance)
            ix1 = self._check_forecast_age(max_forecast_age=max_forecast_age)
            ix2 = self._check_cyclone_category(tc_category=tc_category  , max_days=max_days)
            ix3 = self._check_over_sea()
            ix = ix0 & ix1 & ix2 #& ix3
            self.df.loc[ix, 'GOOD'] = 1
            self.df['GOOD'] = self.df.GOOD.map(lambda x:x*'*')

            dd = self._highlight(max_distance, max_forecast_age, max_days, tc_category, columns)
            return dd
        else :
            return pd.DataFrame()   
    
    def _check_over_sea(self):
        return self.df ['OVER_SEA'] == True
        
    def _check_closest_approach_distance(self, max_distance=220):
        """Check if the closest approach distance between the hurricane position and the mid-swath SAR 
            is lower than the threshold given in km"""
    
        ix= (self.df ['DISTANCE_TO_MID_SWATH[km]'] <= max_distance)
        return ix
        
    def _check_forecast_age(self, max_forecast_age=4):
        """Check if the age of the forecast is lower than the threshold"""

        ix = self.df ['FORECAST_AGE'] <= max_forecast_age
        return ix
      

    def _check_cyclone_category(self, tc_category=0, max_days=1 ):
        """Check the category of cyclone is at least higher than "tc_category" for a period of +/- max_days around the closest approach
        
        t0=TCA-max_days                                               t1=TCA+max_days
        |------------------------- TCA ----------------------------|
            x    x    x     x       *      x    x      x      x       cyclone position
            -1   -1   -1    -1            0   -1    -1      -1
                                          ^
                                        match
        
          """
        ix = self.df.MISSION == 'DUMMY'
        # loop on all closest approached identified
        for rec_ix, rec in self.df.iterrows():
            # get the time of closest approach
            t0, t1 = rec.CYC_UTC_TIME - timedelta(days=max_days), rec.CYC_UTC_TIME + timedelta(days=max_days)
            # get all the hurricane points around the TCA 
            traj_df = self.track.df [(self.track.df.TIME >= t0) & (self.track.df.TIME <= t1)]
            if traj_df.CATEGORY.max() >=tc_category:
                ix.loc[rec_ix] = True
        return ix
            
    def parse_kml_balloon(self, kml_file:str=None):
        """ Parse a the ballonk KML file to return a geopdandas data frame
        
        """
        
        df = []
        reg = '\s+|,'  
        if kml_file != None:
            kml_file = Path(kml_file)
        else:
            kml_file = list(self.wdir.glob(f"{self.mission}_{self.mode}*.KML"))[0]
        
        logger.info(f"Found KML file : {kml_file.name}")
        # opening kml file if exists
        if kml_file.exists():
            root= etree.parse(str(kml_file)).getroot()
            
            # Parse all the folders
            for folder in root.findall('.//Folder', root.nsmap):
                # the result will be placed in a dict and then convert to a data frame
                dd = {}
                
                # Each folder contains 3 placemarks for balloon, circle and SAR
                # The parsing is heavily hardcoded as 
                for ix, pmk in enumerate(folder.findall('./Placemark', root.nsmap)):
                    xps = ['.//name', './/coordinates']
                    val = [ pmk.find(xp, root.nsmap).text for xp in xps ]
                    id_ = val[0].strip()
                    
                    #parsing the coordinates
                    coord = re.split(reg, val[1].strip())
                    coord = [float(cc) for cc in coord ]
                    lon = coord[::3]
                    lat = coord[1::3]      
                    # todo : use shapely directly
                    if ix == 0:
                        geo = Point(lon[0], lat[0])
                    else:
                        coords = [ Point(lon_, lat_)  for lon_, lat_ in zip(lon, lat) ]
                        geo = LineString(coords)

                    dd [f"id_{ix}"] = id_
                    dd [f"geo_{ix}"] = geo        
                    
                    # parse the time of closest approach
                    if ix ==2:
                        sar_mode, sar_pass, time = re.split('_+', id_)
                        time = parse(time)
                        dd ['TCA'] = time
                        
                # convert to pandas and append
                df_ = pd.DataFrame.from_dict(dd, orient='index')
                df.append(df_.T)
            
            # merge all and cleanup
            if len(df):
                df = pd.concat(df, ignore_index=True)
                cols =dict(zip(['id_0', 'geo_0', 'id_1', 'geo_1', 'id_2', 'geo_2'], ['balloon_id', 'balloon_geometry', 'circle_id', 'circle_geometry', 'sar_id','sar_geometry']))
                df.rename(columns=cols, inplace=True)
                return df
            else:
                return pd.DataFrame()            
        else:
            return pd.DataFrame()
    
    def _highlight(self, max_distance, max_forecast_age, max_days, tc_category, columns):

        def format_landflag(val):
            condition = (val == True)
            font_color = 'green' if condition else 'red'
            font_weight = 'bold' if condition else 'normal'

            return 'color: {}; font-weight: {}'.format(font_color, font_weight)        
        def format_forecast_age(val):
            condition = (val <= max_forecast_age)
            font_color = 'green' if condition else 'black'
            font_weight = 'bold' if condition else 'normal'
            return 'color: {}; font-weight: {}'.format(font_color, font_weight)


        def format_dca(val):
            condition = (val <= max_distance)
            font_color = 'green' if condition else 'black'
            font_weight = 'bold' if condition else 'normal'
            return 'color: {}; font-weight: {}'.format(font_color, font_weight)


        def format_cat(val):

            condition = (val >= tc_category)
            font_color = 'green' if condition else 'black'
            font_weight = 'bold' if condition else 'normal'
            return 'color: {}; font-weight: {}'.format(font_color, font_weight)


        def highlight_rows(row):
            value = row.loc['GOOD']
            if value == '*':
                color = '#FFB3BA' # Red
            else:
                color = '#FFFFFF' # Blue
            return ['background-color: {}'.format(color) for r in row]


        dd = self.df[columns].style.apply(highlight_rows, axis=1).\
            applymap(format_forecast_age, subset=['FORECAST_AGE']).\
            applymap(format_dca, subset=['DISTANCE_TO_MID_SWATH[km]']).\
            applymap(format_cat, subset=['CATEGORY']).\
            applymap(format_landflag, subset=['OVER_SEA'])
            
        return dd          