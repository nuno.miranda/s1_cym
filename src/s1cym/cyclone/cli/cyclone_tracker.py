#!/usr/bin/env python
import argparse
from pathlib import Path
from datetime import datetime, timedelta
from dateutil import parser as time_parser
import pandas as pd
import tempfile
from time import sleep
import tomllib as toml
import os


from ..tracker import CycloneTracker
from ..trajectory import HurricaneTrajectory
from ..tc_track import TCTracks
from ..logger._logger import get_logger
from ..mail import SimpleMailer as SimpleMailer

# test urls to use using the -z option
TEST_URLS =['https://diss.ecmwf.int/ecpds/home/wmo/20230210120000/A_JSXX01ECMF101200_C_ECMP_20230210120000_tropical_cyclone_track_DINGANI_83degE_-16degS_bufr4.bin',
 'https://diss.ecmwf.int/ecpds/home/wmo/20230210120000/A_JSXX02ECMF101200_C_ECMP_20230210120000_tropical_cyclone_track_FREDDY_106p7degE_-14p8degS_bufr4.bin',
 'https://diss.ecmwf.int/ecpds/home/wmo/20230210120000/A_JSXX03ECMF101200_C_ECMP_20230210120000_tropical_cyclone_track_GABRIELLE_162p3degE_-24p9degS_bufr4.bin']

TEST_URLS =['https://diss.ecmwf.int/ecpds/home/wmo/20230330060000/A_JSXX01ECMF300600_C_ECMP_20230330060000_tropical_cyclone_track_HERMAN_104p7degE_-15p2degS_bufr4.bin']
logger = get_logger(__name__)

help_string="""Example of use:\n
                cyclone_tracker -s S1A S1B -m EW """


def main():
    arg_parser = argparse.ArgumentParser(prog='cyclone_tracker', usage='%(prog)s [options]',
                                     description='Find intersection of S-1 swath with a cyclone track. If no KML tracks is provided the ECMWF will be used. The track will be resample to provide a point every hour',
                                     add_help=True,
                                     epilog=help_string)

    arg_parser.add_argument('-s', nargs='*', dest="spacecraft", help='spacecraft id list S1[A-D], default:S1A ', default=["S1A"])
    arg_parser.add_argument('-m', nargs='?', dest="mode", help='S-1 instrument mode, default:EW',default='EW')
    arg_parser.add_argument('-r', nargs='?', dest="radius", help='Search radius in meters, default:220Km', default=220e3 )
    arg_parser.add_argument('-t', nargs='?', dest="max_time_diff", help='Search time in fraction of day, default:30min ', default=0.5/24 )
    arg_parser.add_argument('-f', nargs='?', dest="max_forecast_age", help='Maximum age time of the track to consider in hours, default:4*24 ', default=4*24 )
    arg_parser.add_argument('-k', nargs='?', dest="kml_track", help='Cyclome KML track')
    
    arg_parser.add_argument('-n', dest="no_interpolation", help='perform no interpolation of the track ',  action='store_true') 
    arg_parser.add_argument('-e', dest="no_email", help='Do not send email',  action='store_true') 
    arg_parser.add_argument('-z', dest="test_url", help='Test using specific test ECMWF forecast',  action='store_true') 
    
    args = arg_parser.parse_args()
    logger.info (f"Running with arguments {args} ")
    
    t1 = datetime.now()

    logger.info (f"Initialise email")
    home = os.getenv("HOME")
    logger.info (f"Reading RC file")
    with open(f"{home}/.s1cymrc", "rb") as f:
        rc_file = toml.load(f)
        stmp_rc = rc_file['smtp']
    
    mailer = SimpleMailer( stmp_rc['email_host'], stmp_rc['email_port'], stmp_rc['email_host_user'], stmp_rc['email_host_password'])
    mailer.create(stmp_rc['email_to'])
    mailer.add_text("S1_Hurricane_Watch")

    if args.kml_track == None:
        logger.info (f"Downloading from ECMWF...")
        if args.test_url:
            urls = TEST_URLS
        else:
            # In the case no KML file is provided then the ECMWD dissemination data store parsed
            # Only active cyclones are passed to the next step
            active_cyclones = TCTracks.get_active_tropical_cyclones()
            urls = list(active_cyclones.values())
        htracks = [ HurricaneTrajectory.from_ecmwf_bufr(url)  for url  in  urls ]
        htracks = [ htrack for htrack in htracks if htrack != None ]

    else:
        track = Path(args.kml_track)
        logger.info (f"Loading hurricane track {track.name}")
        if track.exists():
            htracks =  [ HurricaneTrajectory.from_cls_kml (track ) ]
    
    if len(htracks):
        #dataframe containing all the potential closest approah with TC
        df = []
        for htrack in htracks:
            logger.info (f"Working on {htrack.name}")
            # Interpolate at 1h sampling to allow better bracketing the TC in azimuth direction
            if args.no_interpolation == False:
                try:
                    htrack = htrack.interpolate()
                except:
                    logger.info("Interpolation Error")
                    return 1
            
            for spacecraft in args.spacecraft:
                logger.info(f"Computing for mission {spacecraft} ")
                #instanciate and run in a temporary directory
                tracker = CycloneTracker(spacecraft, args.mode, htrack, radius=args.radius, max_tca_diff=args.max_time_diff)
                
                with tempfile.TemporaryDirectory() as tmpdirname:
                    df_=tracker(wdir=tmpdirname, max_forecast_age=int(args.max_forecast_age))
                    df_s = tracker.define_quality()
                    kmz_name = tracker.to_kml()
                    if mailer is not None:
                        mailer.attach_zip(kmz_name)
        
                if len(df_) > 0:
                    df.append(df_)
                    logger.info(f"Found for {spacecraft} {args.mode} : {len (df_)} possible hits")
                else:
                    logger.info(f"Found for {spacecraft} {args.mode} : 0 possible hits")

        # At this stage all the storms have been analysed
        # we need now to assess which are the good ones to pass to the planners        
        df = pd.concat(df, ignore_index=True)
        
        df.sort_values(by=['SAR_UTC_TIME', 'GOOD', ], inplace=True, ascending=False)
        
        log_list = [ 'GOOD', 'CAT_NAME',  'NAME', 'MISSION', 'MODE',
                    'ABS_ORBIT', 'REL_ORBIT',
                    'SAR_UTC_TIME', 'CYC_UTC_TIME', 
                    'DISTANCE_TO_MID_SWATH[km]', 'TIME_DIFF_VS_TRACK[MIN]', 'FORECAST_AGE', 'OVER_SEA']
        logger.info (f"\n{df[log_list]}")


        dd = df[log_list]
        ix = dd.GOOD == '*'
        title = f"[S-1 Hurricane Watch] : {ix.sum()}  hits found"
        txt = dd.to_string()
        mailer.subject=title
        mailer.add_text(txt)
        mailer.send()
    else:
        logger.info(f"No active cyclones found")
    
        title = f"[S-1 Hurricane Watch] : no  hit found"
        txt = ""
        mailer.subject=title
        mailer.add_text(txt)
        mailer.send()


if __name__ == "__main__":
    main()