from typing import Union
from datetime import datetime,date
import smtplib
from pathlib import Path
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.base import MIMEBase
from email import encoders
import zipfile
from email.message import EmailMessage

from ..logger._logger import get_logger
logger = get_logger(__name__)

class SimpleMailer():

    def __init__(self, email_host, email_port, email_host_user, email_host_password):
        logger.info (f'Mailer Opened with {email_host=} {email_port=} {email_host_user=} {email_host_password=}')
        self.session = smtplib.SMTP(email_host, email_port) #use gmail with port
        self.session.starttls() #enable security
        self.session.login(email_host_user, email_host_password) #login with mail_id and password
        self.text=[]
        self.html=[]
        

    @property
    def subject(self):
        return self.message['Subject']

    @subject.setter
    def subject(self,subject):
        self.message['Subject'] = subject

    def create(self,to:str, subject:str=None, sender:str=None)->None:
        
        self.message = MIMEMultipart()
        sender = sender if sender != None else self.session.user
        self.message['From'] = sender
        self.message['To'] = to
        self.message['Subject'] = subject



    def add_text(self, text:str) ->None:
        self.text.append(text)
       
    def add_html(self, html:str) ->None:
        self.html.append(html)

    def attach_image(self, image_file:Union[str, Path])->None:
        image_file = Path(image_file)

        with open(image_file, 'rb') as f:
            logger.info(f"Attaching image : {f.name}")
            
            # set attachment mime and file name, the image type is png
            mime = MIMEBase('image', 'png', filename=image_file.name)
            # add required header data:
            mime.add_header('Content-Disposition', 'attachment', filename=image_file.name)
            mime.add_header('X-Attachment-Id', '0')
            mime.add_header('Content-ID', '<0>')
            
            # read attachment file content into the MIMEBase object
            mime.set_payload(f.read())
            
            # encode with base64
            encoders.encode_base64(mime)
            # add MIMEBase object to MIMEMultipart object
            self.message.attach(mime)


    def attach_text(self, text_file:Union[str, Path])->None:
        text_file = Path(text_file)
        logger.info(f"KML file {text_file} ")

        with open(text_file) as f:
            attachment = MIMEText(f.read())
            attachment.add_header('Content-Disposition', 'attachment', filename=text_file.name)           
            self.message.attach(attachment)

    def attach_zip(self, zip_file:Union[str, Path])->None:
        zip_file = Path(zip_file)

        if zip_file.exists != None:
            mime = MIMEBase('application', "octet-stream")
            mime.add_header('X-Attachment-Id', '0')
            mime.add_header('Content-ID', '<0>')
            mime.set_payload(open(zip_file, 'rb').read())
            encoders.encode_base64(mime)
            mime.add_header('Content-Disposition', 'attachment', filename=zip_file.name)
            self.message.attach(mime)
   

    def send(self):
        logger.info(f"email sent")
        body = "\n".join(self.text)
        body = MIMEText(body, 'plain')
        self.message.attach(body)

        if len(self.html):
            body = "\n".join(self.html)
            body = MIMEText(body, 'html')
            self.message.attach(body)
            
        text = self.message.as_string()
        self.session.sendmail(self.message['From'], self.message['To'], text)
    

    def __call__(self,sender=None, to=None, subject=None, text=None, zip_file = None, image_file=None):
        
        self.create(to, subject, sender=sender)

        self.add_text(text)

        if image_file != None:
            self.attach_image(image_file)

        if zip_file != None:
            self.attach_zip(zip_file)
            
        self.send()
        
    def __exit__(self):
        logger.info ('Mailer Closed')
        self.session.quit()


        
    def __exit__(self):
        logger.info ('Mailer Closed')
        self.session.quit()



