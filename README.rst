S-1 Cyclone Tracker python tool
===========================================
:Copyright: 2020-2023, Nuno Miranda  <nuno.miranda.@esa.int>

.. badges

.. contents:: Overview
   :depth: 3

.. description

The Sentinel-1 Cyclone Monitoring ``s1cym`` Python package propagates the Sentinel-1 Orbit Scenario file along the trajectory
of Tropycal cyclones to find potential intersection with a specific swath defined by a swath definition file.

The *orbit Scenario files* and *swath definition files* can be found at here_.

.. _here: https://eop-cfi.esa.int/index.php?option=com_content&view=article&id=117&Itemid=668&jsmallfib=1&dir=JSROOT/ORBIT_SWATH_DATA

Swath definition files are static and orbit scenario files are changing in a yearly basis.


The package provides classes and functions that can be used to:

* load and manipulate Cyclone trajectory from different sources
* Perform propagation along the trajectory. The core of the propagation is ensure by the ESA *AeolusBalloonPass*  tool in the frame of the ESA Aeolus mission. This is a binary that can be found in the EOCFI_ page.

.. _EOCFI: https://eop-cfi.esa.int/index.php/applications/tools/command-line-tools-aeolus-balloon-pass

Installation
-------------
Installing *AeolusBalloonPass*  
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
- Download the tarball matching your environment  and extract it in place referreed as ``AeolusBalloonPass_PATH``
- Create a gloval environemnt valriable ``CYCLONE_TRACKER_CFI`` accessible from python. This can be done by placing following command in the ~/.bash_profile:



``export CYCLONE_TRACKER_CFI="AeolusBalloonPass_PATH/AeolusBalloonPass_LINUX64_v1_1_1_date_20_FEB_2022/AeolusBalloonPass"``

Installing the s1cym package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
To install the package run the  following code pip command:

.. code-block:: bash

    $ pip install git+https://gitlab.com/nuno.miranda/s1_cym

Or 


.. code-block:: bash
    
    $ git clone https://gitlab.com/nuno.miranda/s1_cym
    $ cd s1_cym
    $ pip install .


It is advised to create a dedicated virtual environemnt first using pip or conda

Usage in the shell
-------------------
The ``s1cym`` package comes with a command line interface allowing to run 
the detection of overpasses over hurricanes directly from the shell.
This is done thanks to the ``cyclone_tracker``  tool.

Please note that from jupyter the environemnt variable CYCLONE_TRACKER_CFI won't be avalable by default.
In order to make it work you need either to:

 - declare the variable in the notebook using the magic ``%env CYCLONE_TRACKER_CFI=AeolusBalloonPass_PATH/AeolusBalloonPass_LINUX64_v1_1_1_date_20_FEB_2022/AeolusBalloonPass``
 - declare the variable in the ~/.bash_profile

.. code-block:: bash
    
    $ cyclone_tracker --help
    usage: cyclone_tracker [options]

    Find intersection of S-1 swath with a cyclone track

    options:
    -h, --help           show this help message and exit
    -s [SPACECRAFT ...]  spacecraft id list S1[A-D], default:S1A
    -m [MODE]            S-1 instrument mode, default:EW
    -k [KML_TRACK]       Cyclome KML track
    -r [RADIUS]          Search radius in meters, default:200Km
    -t [MAX_TIME_DIFF]   Search time in fraction of day, default:3H

As an example the following instruction are valid.

.. code-block:: bash

    $ cyclone_tracker.py -s S1A  -k JUDY_15_P_bufr4_track_only.kml 
    $ cyclone_tracker.py -s S1A  -m EW -k JUDY_15_P_bufr4_track_only.kml 
    $ cyclone_tracker.py -s S1A  S1B -m EW -k JUDY_15_P_bufr4_track_only.kml -r 100e3


Usage as python class
---------------------
For the usage in notebooks examples are given in the docs folder of the package. 

.. code-block:: python
    
    from s1cym import HurricaneTrajectory, CycloneTracker

    kml_file = 'JUDY_15_P-A_JSXX02ECMF270000_C_ECMP_20230227000000_tropical_cyclone_track_JUDY_170p4degE_-12p2degS_bufr4_track_only.kml'
    trajectory = HurricaneTrajectory.from_cls_kml (kml_file )
    tracker = CycloneTracker("S1A", "EW", trajectory)
    
    #import result in a data frame
    df =trk()

 
